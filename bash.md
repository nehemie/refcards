Bash Refcard
=================

   * [Manipulating script](#manipulating-script)
      * [Add ~/bin to bash](#add-bin-to-bash)
      * [Make shell exectuable](#make-shell-exectuable)
   * [Manipulating text-file](#manipulating-text-file)
      * [Play with lines](#play-with-lines)
         * [Add a line at a specific position](#add-a-line-at-a-specific-position)
         * [Add a line to multiple files](#add-a-line-to-multiple-files)
         * [Insert a line before every line with the pattern](#insert-a-line-before-every-line-with-the-pattern)
         * [Replace a line which matches the pattern](#replace-a-line-which-matches-the-pattern)
         * [Confirmation before each replacement](#confirmation-before-each-replacement)
      * [Converting a Word Document to Markdown](#converting-a-word-document-to-markdown)
   * [Disk management](#disk-management)
      * [Wipe a disk](#wipe-a-disk)
      * [Create an encrypted volume](#create-an-encrypted-volume)

-----


Manipulating script 
-------------------

## Running a Script on connecting USB device <!-- https://unix.stackexchange.com/a/28577 -->

1. get info on your /dev/sdX device

    udevadm info -a -n sdX

2. Put a line like this in a file in `/etc/udev/rules.d`

    KERNEL=="sd*", ATTRS{vendor}=="Yoyodyne", ATTRS{model}=="XYZ42", ATTRS{serial}=="123465789", RUN+="/pathto/script"

Manipulating text file
----------------------


### Play with lines


#### Insert a line before every line with the pattern

    sed '/PATTERN/ i Line which you want to insert' input.txt > output.txt 


#### Replace a line which matches the pattern

    sed '/PATTERN/ c Line you want to insert'  input.txt > output.txt 


#### Confirmation before each replacement

use vim!

    vim -c '%s/PATTERN/NEWPATTERN/gc' -c 'wq' FILE_NAME 

looped in a multiple filles? First populate the argument `:args` or the buffer
`:bufdo`

    :args **/*.rb

will recursively search the current directory for ruby files. Notice that this
is also like opening Vim with `vim **/*.rb`. For files in current folder `vim *`

Get a list of all files in the current directory :args

To add or delete files from the list, you can use the `argadd` or the
`:argdelete` commands respectively. `:argdo` command which runs a command for
every file in the argument list

    :argdo %s/search/replace/gc | update

`:update` is used because it will only save the file if it has changed. Add a
`/e` search flag to skip the "pattern not found" errors



### Converting a Word Document to Markdown

    unoconv -f html test.docx pandoc -f html -t markdown -o test.md test.html






Disk management
--------------

### Wipe a disk

Writing a zero byte to every addressable location on the disk. `iflag` and
`oflag` try to disable buffering, which is senseless for a constant stream.

    dd if=/dev/zero of=/dev/sdX iflag=nocache oflag=direct bs=4096 

[source](https://wiki.archlinux.org/index.php/Securely_wipe_disk#dd)

Monitor the progress of dd: In **second** terminal:

    watch -n5 'sudo kill -USR1 `pgrep ^dd`' 

[source](http://www.serenux.com/2011/02/howto-monitor-the-progress-of-dd/)


### Create an encrypted volume
[source: fsmithred](http://www.debianuserforums.org/viewtopic.php?f=9&t=408/url)

    # Hier sdc1!
    # sudo umount /dev/sdc1 
    # sudo cryptsetup luksFormat /dev/sdc1 
    # sudo cryptsetup luksOpen /dev/sdc1 <name>

<name> is a temporary name for the partition. It only exists until you close the
volume. Avoid special characters and spaces.

    # sudo mke2fs -t ext4 /dev/mapper/<name>
    # mount /dev/mapper/<name> /mnt
    # sudo chown -R user:user /mnt


### Install a hybrid iso on an usb

     dd if=xubuntu-16.04.4-desktop-amd64.iso of=/dev/sdXXXX bs=4M; sync

