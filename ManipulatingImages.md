Manipulating Images
===================

    * [setup](#setup)
      * [Get infos](#get-infos)
    * [Convert](#convert)
      * [SVG to PNG](#svg-to-png)
      * [PNG to TIFF](#png-to-tiff)
      * [PDF to TXT](#pdf-to-txt)
-----


Author: Néhémie Strupler
License: CC BY-SA 4.0



Set-up
-----

-   Inkscape
-   ImageMagick
-   Tesseract-ocr




Inspect
------

### Get infos

    identify -verbose image.svg 





Convert
------

### SVG to PNG

    inkscape image.svg -d 300 -e  image.png

    for f in *.svg; do
      inkscape ./"$f" -d 300 -e ./"${f%.svg}.png"
    done

### PNG to TIFF

For unique file

    convert image.png -compress None image.tiff
    convert image.png -compress JPEG image.tiff

For all files in a folder

    for f in *.png; do
      convert ./"$f" -compress JPEG ./"${f%.png}.tiff"
    done


### PNG to PDF

    convert image*.png image.pdf

### PDF to TXT

Convert PDF to PNG and PNG to TXT 

    convert -density 300 -depth 8 image.pdf image.pdf
    tesseract image.png text.txt -l lan[guage: deu,fra,eng,tur]




Combine
-------

    montage img.jpg img_right.jpg -geometry +0+0 images.jpg

for arrangement use tile (here image on top of the other)
    
    montage img.jpg img_right.jpg -geometry +0+0 -tile 1x2 images.jpg



Reduce
------

Redue jpg in folder for 50%

    mogrify -path imagepath  -strip -quality 50% *.jpg
