


Extract / modify PDF metadata (using pdftk)
--------------------------------------------------------

### export metadata

    pdftk original.pdf dump_data output data.txt

### edit metadata

    vim data.txt

### import new metada

    pdftk original.pdf update_info data.txt output end.pdf


Splitt PDF (two scanned pages)
------------------------------

    mutool poster -y 2 input.pdf output.pdf


OCR
---

ruby  ~/bin/pdfocr.rb  -i input.pdf -o output.pdf
