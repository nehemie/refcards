Table of Contents
=================


-------
### Table of Contents

  * [Dealing with files](#dealing-with-files)
    * [Viewing differences](#viewing-differences)
      * [between two commit](#between-two-commit)
      * [Between the two last commits](#between-the-two-last-commits)
    * [Inspecting History](#inspecting-history)
    * [Deleting files](#deleting-files)
      * [Deleting in Remote and index but keep-local](#deleting-in-remote-and-index-but-keep-local)
      * [Finding a file](#finding-a-file)
      * [Restoring a file](#restoring-a-file)
  * [Dealing with commits](#dealing-with-commits)
    * [Delete last commit and never see it again](#delete-last-commit-and-never-see-it-again)
  * [Branches](#branches)
      * [Visualizing branch topology](#visualizing-branch-topology)
      * [Creating and switching to a new specified branch](#creating-and-switching-to-a-new-specified-branch)
      * [Push a new local branch to a remote Git repository and track it](#push-a-new-local-branch-to-a-remote-git-repository-and-track-it)
      * [Copying a file from the branch where you want the file to end up:](#copying-a-file-from-the-branch-where-you-want-the-file-to-end-up)
      * [Deleting a branch locally](#deleting-a-branch-locally)
      * [Deleting a remote branch](#deleting-a-remote-branch)
  * [Create repository](#create-repository)
    * [Push existing folder or Git repository](#push-existing-folder-or-git-repository)
  * [Configuring Git](#configuring-git)
      * [User information for all local repositories](#user-information-for-all-local-repositories)
  * [git-lfs](#git-lfs)
    * [Move from git-lfs to git](#move-from-git-lfs-to-git)


-----



Dealing with files
------------------


### Viewing differences

#### between two commit

    git diff $start_commit..$end_commit -- path/to/file.extension

#### Between the two last commits

    git diff HEAD^ HEAD path/to/file.extension

#### Between two branches from tip

    git diff branche1..branche2

#### Between two branches from common ancestor

    git diff branche1...branche2

### Inspecting History

    git log --follow file.txt




### Deleting files

#### Deleting in Remote and index but keep-local

    git rm --cached mylogfile.log

#### Finding a file

    git log --all -- path/to/file.extension

#### Restoring a file

    git checkout <SHA>~1 -- path/to/file.extension


#### Hard reset of a single file

     git checkout HEAD -- my-file.txt

Dealing with commits
--------------------

### Delete last commit and never see it again

    git reset --hard 'HEAD~1'







Branches
--------

#### Visualizing branch topology

    git log --graph --decorate --oneline

#### Creating and switching to a new specified branch

    git checkout -b [branch-name]

#### Push a new local branch to a remote Git repository and track it

    git push -u origin [branch-name]

#### Copying a file from the branch where you want the file to end up:

    git checkout [otherbranch] [path/myfile.txt]

#### Deleting a branch locally

    git branch -d [branch-name]

#### Deleting a remote branch

    git push origin :[branch-name]

#### Rename branch (local + remote)

    git rename -m [new bramch-name]
    git push origin :[old branch-name]
    git push --set-upstream origin [new branch-name]



## Clone

### Shallow clone: history truncated to specified number (1) of commits

    git clone --depth 1 [repository_url]


Create repository
----------------------

### Push existing folder or Git repository

    cd existing_folder
    git init
    git remote add origin git@[host.com]:[user]/[repository_name].git
    git push -u origin master

Tags
----

To create your tag:

    git tag v.0.0.0

Annotated Tags + Signed

    git tag -a v.0.0.0 -m "Zero versio" -s

To push local tags to remote:

    git push --tags

Another option

    git push --follow-tags

To delete a local tag:

    git tag -d released/aug2016

To delete a remote tag:

    git push origin :tagname




Configuring Git
--------------------

### User information for all local repositories

    git config --global user.name "[name]"
    git config --global user.email "[email address]"
    git config --global color.ui auto

### Vimdiff as mergetool

    git config merge.tool vimdiff
    git config merge.conflictstyle diff3
    git config mergetool.prompt false

git-lfs
-------

### Move from git-lfs to git

   git-lfs untrack [file]
   git-lfs checkout
   git status
