From 
 - https://help.github.com/articles/generating-ssh-keys/
 - https://help.ubuntu.com/community/SSH/OpenSSH/Keys



# Step 1: Check for SSH keys

First, we need to check for existing SSH keys on your computer. Open the command line and enter:

```
ls -al ~/.ssh
# Lists the files in your .ssh directory, if they exist
```



# Step 2: Generate a new SSH key

    With the command line still open, copy and paste the text below. Make sure you substitute in your GitHub email address.

```
ssh-keygen -t rsa -b 4096 -C "nehemie.strupler@etu.unistra.fr"
# Creates a new ssh key, using the provided email as a label
# Generating public/private rsa key pair.
#  "rsa" (Rivest-Shamir-Adleman)
#  Note: The default is a 2048 bit key. You can increase this to 4096 bits with the -b flag 
# (Increasing the bits makes it harder to crack the key by brute force methods). 
```





# Step 3: Add your key to the ssh-agent

```
    # start the ssh-agent in the background
    eval "$(ssh-agent -s)"
    # Agent pid 59566
```
    Add your SSH key to the ssh-agent:
```
    ssh-add ~/.ssh/id_rsa
```





# Step 4: Add your SSH key to your account
cat ~/.ssh/id_rsa.pub