Readme
--------------


Configuration for mirror:
httpredir.debian.org acts as a Debian mirrors redirector and "allows many
of the nearly 400 Debian mirrors to be available via a single address,
adapting to your network location, IP family connectivity, and service
availability", says Raphael.

To use this service with Debian Jessie, just put
deb http://httpredir.debian.org/debian jessie main
in your
/etc/apt/sources
file.


## Configure keyboard-layout

edit  /etc/default/keyboard

------------------------------------------
    # KEYBOARD CONFIGURATION FILE

    # Consult the keyboard(5) manual page.

    XKBMODEL="pc105" # May change
    XKBLAYOUT="tr,ch"
    XKBVARIANT=""
    XKBOPTIONS="grp:alt_shit_toogle"

    BACKSPACE="guess"
------------------------------------------

## Change Timezone

    dpkg-reconfigure tzdata

## Make zsh default

    chsh -s $(which zsh)


## List installed package by date

    grep install /var/log/dpkg.log

## Install .deb

    sudo dpkg -i /path/to/deb/file
    sudo apt-get install -f


## Pass

### View password store

    pass

### View passwrod

    pass web/sitename

###  Initialise password store:

    pass init <gpg-id or email>

### Create a new password

Provide a descriptive hierarchical name

    pass insert web/sitename

### Generate a new password

    pass generate web/sitename n


WIFI
----

    sudo ifconfig wlan0 up

Scan for a list of WiFi networks in range:

    sudo iwlist wlan0 scan


pick yours from the list:

     sudo iwconfig wlan0 essid WifiNAME key s:PASSWORD


To obtain the IP address, now request it with the Dynamic Host Client:

    sudo dhclient wlan0


Disable
     sudo ifconfig wlan0 down


### Connecting with wpa_Supplicant


Need a configuration file at /etc/wpa_supplicant.conf
launch it with dhclient

    wpa_supplicant -Dnl80211 -iwlp2s0 -c/etc/wpa_supplicant.conf
    dhclient wlp2s0



RESCUE
------

###  Recover deleted data from a USB flash drive?

> https://unix.stackexchange.com/a/288136/179383

Enter TestDisk:

    sudo testdisk /dev/sdb

It will show you a step-by-step procedure through a TUI (textual user interface). The essential steps are:

    scanning the drive
    selecting the partition
    pressing "P" to show the files
    copying the deleted (red) files with "C"

### Install of 2018

<!-- Inspired by https://xo.tc/setting-up-full-disk-encryption-on-debian-jessie.html -->

Start was the debian 64-bit PC netinst iso in "non graphical" mode.

#### Manually partition disk. 
  * /boot Unencrypted
  * /boot/efi
  * /root volume, encrypted with a passphrase.
  * /home Encrypted with a key file (that's stored on /).
  *  SWAP Encrypted with a random key generated each time we boot.

How to: Select disk to partition. If it's a new disk create a partition
table or wipe the existing one. Create a new partition  Primary
Partition for /boot (256MB)Change the mount point to /boot. Next setup
root volume with 64GB. Logical.  Change "use as" into "physical volume
for encryption". Do the same for /home (full disk minus 4 GB for swap)
Finally create a partition that we will (eventualy) use for swap.
However simply create a partition, and in "use as" set it to "do not
use the partition". Now select "Configure encrypted volumes". After it
select the encrypted volume and map them to / and /home and change
format to btrfs

Setup the swap partition to use a random key at boot. We need to edit
    /etc/crypttab
add
    sda5_crypt /dev/sda5 /dev/urandom swap

Then we need to edit
    /etc/fstab
and add
    /dev/mapper/sda5_crypt none swap sw 0 0

Passphrase for our home partition: generate file with random data

    mkdir /etc/keys
    dd if=/dev/random of=/etc/keys/sda4.key bs=1 count=32
    chmod 400 /etc/keys/sda6.key

add key to decrypt that volume

    sudo cryptsetup luksAddKey /dev/sda4 /etc/keys/sda4.key

remove current passphrase

    sudo cryptsetup luksRemoveKey /dev/sda4

edit

    /etc/crypttab

on the line that has sda4_crypt replace the word none with

    /etc/keys/sda4.key

Now boot.

## USB Tethering

Connect USB + set USB Tethering

Lauch interface and dhclient

    sudo ip link set enp0s20u3 up
    sudo dhclient enp0s20u3                                            :(


## Relaunch fix ip

    sudo ifdown eno1
    sudo ip addr flush eno1
    sudo ifup eno1

    sudo ifdown eno1 && sudo ip addr flush eno1 && sudo ifup eno1



