## `siunitx` for the impatient

The package provides the user macros: 
 - \ang[options]{angle}
 - \num[options]{number}
 - \si[options]{unit}
 - \SI[options]{number}[pre-unit]{unit}
 - \numlist[options]{numbers}
 - \numrange[options]{numbers}{number2}
 - \SIlist[options]{numbers}{unit}
 - \SIrange[options]{number1}{number2}{unit}
 - \sisetup{options}
 - \tablenum[options]{number}
