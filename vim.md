# Learning vim

## Capture the output from running an external (or shell) command in Vim

Insert from bash

    :r(ead) !date

Replace a command with its output

    :<,'>!bash

## Quote a word

    `ciw '' Esc P`

## Insert multiple (n) character

    (Esc) nic Esc Esc

## Windows management

### Swap windows

    `Ctrl-w  H`  or  `:wincmd H` : from **horizontal to vertical** layout.

    `Ctrl-w  J` or  `:wincmd J`  : from **vertical to horizontal** layout.

    `Ctrl-w R` or type `:wincmd r`:  **swap the two buffers**but keep the window
     layout the same.

### Move an existing window to a new tab?

     Ctrl-W Shift-T
     :tabedit %


## Delete every line that doesn't contain "price"

    :%g!/price/d

## Toggle case ~, lowercase u, uppercase U

    ~  Toggle case of the character under cursor, or selected

    g~iw Toggle case of the current word .

    g~~ Toggle case of the current line (same as V~).

## Open multiple files at once from vim?

    :next *.txt

## Insert non breaking space

Use the proper UTF-8 character for a non-breaking space (U+00A0)
In Vim, you can insert it with (https://stackoverflow.com/a/31570934/2759357)

    Ctrl-v x a 0
