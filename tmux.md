# tmux refcard

(C-x means ctrl+x, M-x means alt+x)

## Profile config

see ~/.tmux.conf.
 - Prefix defined as Control + a = C-a

## Window

create a new window

    tmux new-window (prefix + c)

move to the window based on index

    tmux select-window -t :0-9 (prefix + 0-9)

rename the current window

    tmux rename-window (prefix + ,)

## Pane

Creating a new pane by splitting an existing one:

    C-a "          split vertically (top/bottom)
    C-a %          split horizontally (left/right)

Switching between panes:

    C-a left       go to the next pane on the left
    C-a right      (or one of these other directions)
    C-a up
    C-a down
    C-a o          go to the next pane (cycle through all of them)
    C-a ;          go to the ‘last’ (previously used) pane

Moving panes around:

    C-a {          move the current pane to the previous position
    C-a }          move the current pane to the next position
    C-a C-o        rotate window ‘up’ (i.e. move all panes)
    C-a M-o        rotate window ‘down’
    C-a !          move the current pane into a new separate
                   window (‘break pane’)
    C-a :move-pane -t :3.2
                   split window 3's pane 2 and move the current pane there

Resizing panes:

    C-a M-up, C-a M-down, C-a M-left, C-a M-right
                   resize by 5 rows/columns
    C-a C-up, C-a C-down, C-a C-left, C-a C-right
                   resize by 1 row/column

Applying predefined layouts:

    C-a M-1        switch to even-horizontal layout
    C-a M-2        switch to even-vertical layout
    C-a M-3        switch to main-horizontal layout
    C-a M-4        switch to main-vertical layout
    C-a M-5        switch to tiled layout
    C-a space      switch to the next layout


Other:

    C-a x          kill the current pane
    C-a q          display pane numbers for a short while

Copy (perso bindings):

    C-a Escap      enter copy mode
    v              start selection
    y              copy selection
    p              paste selection
